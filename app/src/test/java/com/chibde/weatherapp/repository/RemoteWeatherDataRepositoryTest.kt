package com.chibde.weatherapp.repository


import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.chibde.weatherapp.api.WeatherApiService
import com.chibde.weatherapp.db.WeatherAppDb
import com.chibde.weatherapp.db.dao.LocationDao
import com.chibde.weatherapp.db.dao.WeatherDataDao
import com.chibde.weatherapp.db.entity.WeatherData
import com.chibde.weatherapp.util.TestUtil
import com.chibde.weatherapp.util.mock
import com.chibde.weatherapp.utils.Locations
import com.chibde.weatherapp.utils.Metrics
import com.nhaarman.mockitokotlin2.doAnswer
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import org.mockito.Mockito.times

@RunWith(JUnit4::class)
class RemoteWeatherDataRepositoryTest {
    private lateinit var repository: RemoteWeatherDataRepository
    private val weatherDao = Mockito.mock(WeatherDataDao::class.java)
    private val locationDao = Mockito.mock(LocationDao::class.java)
    private val service = Mockito.mock(WeatherApiService::class.java)

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun init() {
        val db = Mockito.mock(WeatherAppDb::class.java)
        Mockito.`when`(db.weatherAppDao()).thenReturn(weatherDao)
        Mockito.`when`(db.locationDao()).thenReturn(locationDao)
        Mockito.`when`(db.runInTransaction(ArgumentMatchers.any())).thenCallRealMethod()
        repository = RemoteWeatherDataRepository(service, db)
    }

    @Test
    fun verifyDataInsertedFromNetwork() = runBlocking {
        val weatherDataResponse = TestUtil.createDummyWeatherDataResponse()

        enumValues<Locations>().forEach { location ->
            Mockito.`when`(weatherDao.getWeatherDataForLocation(location = location.id))
                .thenReturn(emptyList())
            val weatherData = TestUtil.createWeatherDataFromResponse(location)

            enumValues<Metrics>().forEach { metric ->
                Mockito.`when`(
                    service.getWeatherData(
                        metric = metric.metric,
                        location = location.name
                    )
                ).doAnswer {
                    GlobalScope.async {
                        weatherDataResponse
                    }
                }
            }
            async { repository.getWeatherData(location) }.await()
            Mockito.verify(weatherDao).getWeatherDataForLocation(location.id)
            Mockito.verify(weatherDao).insert(weatherData)
        }
    }

    @Test
    fun verifyNoDataInsertedWhenDataExist() = runBlocking {
        val data = mock<List<WeatherData>>()
        val weatherData = TestUtil.createDummyWeatherData(Locations.England)

        enumValues<Locations>().forEach { location ->
            Mockito.`when`(weatherDao.getWeatherDataForLocation(location = location.id))
                .thenReturn(weatherData)
            repository.getWeatherData(location)

            Mockito.verify(weatherDao).getWeatherDataForLocation(location.id)

            Mockito.verify(weatherDao, times(0)).insert(data)
        }
    }
}