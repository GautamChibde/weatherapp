package com.chibde.weatherapp.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.chibde.weatherapp.db.WeatherAppDb
import com.chibde.weatherapp.db.dao.WeatherDataDao
import com.chibde.weatherapp.db.entity.WeatherData
import com.chibde.weatherapp.util.TestUtil
import com.chibde.weatherapp.util.mock
import com.chibde.weatherapp.utils.Locations
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import org.mockito.Mockito.verify

@RunWith(JUnit4::class)
class LocalWeatherDataRepositoryTest {
    private lateinit var repository: LocalWeatherDataRepository
    private val dao = Mockito.mock(WeatherDataDao::class.java)

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun init() {
        val db = Mockito.mock(WeatherAppDb::class.java)
        Mockito.`when`(db.weatherAppDao()).thenReturn(dao)
        Mockito.`when`(db.runInTransaction(ArgumentMatchers.any())).thenCallRealMethod()
        repository = LocalWeatherDataRepository(db)
    }

    @Test
    fun getWeatherDataFor() {
        val data = MutableLiveData<List<WeatherData>>()
        val observer = mock<Observer<List<WeatherData>>>()

        Mockito.`when`(dao.getWeatherData(year = 1910, location = Locations.England.id))
            .thenReturn(data)

        val weatherData = TestUtil.createDummyWeatherData(Locations.England)

        repository.getWeatherDataFor(year = 1910, location = Locations.England.id)

        verify(dao).getWeatherData(year = 1910, location = Locations.England.id)
        data.observeForever(observer)
        data.postValue(weatherData)
        verify(observer).onChanged(weatherData)
    }
}