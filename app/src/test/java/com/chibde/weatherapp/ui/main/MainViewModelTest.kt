package com.chibde.weatherapp.ui.main

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.chibde.weatherapp.db.entity.WeatherData
import com.chibde.weatherapp.repository.LocalWeatherDataRepository
import com.chibde.weatherapp.util.TestUtil
import com.chibde.weatherapp.utils.Locations
import com.nhaarman.mockitokotlin2.*
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito

@RunWith(JUnit4::class)
class MainViewModelTest {
    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private val repository = Mockito.mock(LocalWeatherDataRepository::class.java)
    private val viewModel = MainViewModel(repository)

    @Test
    fun testNull() {
        assertThat(viewModel.weatherData, CoreMatchers.notNullValue())
        verify(repository, never()).getWeatherDataFor(Mockito.anyInt(), Mockito.anyInt())
        viewModel.setRequestData(2001, 1)
        verify(repository, never()).getWeatherDataFor(Mockito.anyInt(), Mockito.anyInt())
    }

    @Test
    fun testCallRepo() {
        viewModel.weatherData.observeForever(mock())
        viewModel.setRequestData(2001, 1)
        verify(repository).getWeatherDataFor(2001, 1)
    }

    @Test
    fun testSetRequestData() {
        val data = MutableLiveData<List<WeatherData>>()
        Mockito.`when`(repository.getWeatherDataFor(2001, 1)).thenReturn(data)
        val observer = mock<Observer<List<WeatherData>>>()
        viewModel.weatherData.observeForever(observer)

        viewModel.setRequestData(2001, 1)

        verify(observer, never()).onChanged(any())

        val testData = TestUtil.createDummyWeatherData(Locations.England)
        reset(observer)
        viewModel.setRequestData(2001, 1)

        data.postValue(testData)

        verify(observer).onChanged(testData)
    }

    @Test
    fun testNextYear() {
        var year = 2001
        val data = MutableLiveData<List<WeatherData>>()
        year++
        Mockito.`when`(repository.getWeatherDataFor(year, Locations.England.id)).thenReturn(data)
        val observer = mock<Observer<List<WeatherData>>>()
        viewModel.weatherData.observeForever(observer)

        viewModel.nextyear()

        verify(observer, never()).onChanged(any())

        val testData = TestUtil.createDummyWeatherData(Locations.England)

        reset(observer)
        year++
        Mockito.`when`(repository.getWeatherDataFor(year, Locations.England.id)).thenReturn(data)
        viewModel.nextyear()

        data.postValue(testData)

        verify(observer).onChanged(testData)
    }

    @Test
    fun testPreviousYear() {
        var year = 2001
        val data = MutableLiveData<List<WeatherData>>()
        year--
        Mockito.`when`(repository.getWeatherDataFor(year, Locations.England.id)).thenReturn(data)
        val observer = mock<Observer<List<WeatherData>>>()
        viewModel.weatherData.observeForever(observer)

        viewModel.previousYear()

        verify(observer, never()).onChanged(any())

        val testData = TestUtil.createDummyWeatherData(Locations.England)

        reset(observer)
        year--
        Mockito.`when`(repository.getWeatherDataFor(year, Locations.England.id)).thenReturn(data)
        viewModel.previousYear()

        data.postValue(testData)

        verify(observer).onChanged(testData)
    }
}