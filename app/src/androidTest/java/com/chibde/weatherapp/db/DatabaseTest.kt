package com.chibde.weatherapp.db

import androidx.arch.core.executor.testing.CountingTaskExecutorRule
import androidx.room.Room
import androidx.test.InstrumentationRegistry
import com.chibde.weatherapp.db.entity.WeatherData
import com.chibde.weatherapp.util.TestUtil
import com.chibde.weatherapp.utils.Locations
import org.junit.After
import org.junit.Before
import org.junit.Rule
import java.util.concurrent.TimeUnit

abstract class DatabaseTest {
    @Rule
    @JvmField
    val countingTaskExecutorRule = CountingTaskExecutorRule()

    private lateinit var _db: WeatherAppDb
    val db: WeatherAppDb
        get() = _db

    private lateinit var _weatherData: List<WeatherData>
    val weatherData: List<WeatherData>
        get() = _weatherData


    @Before
    fun initDb() {
        _db = Room.inMemoryDatabaseBuilder(
            InstrumentationRegistry.getContext(),
            WeatherAppDb::class.java
        ).build()

        _weatherData = TestUtil.createDummyWeatherData(locations = Locations.England)

    }

    @After
    fun closeDb() {
        countingTaskExecutorRule.drainTasks(10, TimeUnit.SECONDS)
        _db.close()
    }
}