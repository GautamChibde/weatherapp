package com.chibde.weatherapp.db

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.runner.AndroidJUnit4
import com.chibde.weatherapp.db.entity.Location
import com.chibde.weatherapp.utils.Locations
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.notNullValue
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class LocationDaoTest : DatabaseTest() {
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Test
    fun testLocationInsertAndRead() {

        enumValues<Locations>().forEach {
            db.locationDao().insert(
                Location(
                    id = it.id,
                    name = it.name
                )
            )
        }
        val locations = db.locationDao().getAll()

        assertThat(locations, notNullValue())
        assertThat(locations.size, `is`(Locations.values().size))

        val locationForId = db.locationDao().getForId(Locations.England.id)

        assertThat(locationForId, notNullValue())
        assertThat(locationForId.name, `is`(Locations.England.name))
    }
}