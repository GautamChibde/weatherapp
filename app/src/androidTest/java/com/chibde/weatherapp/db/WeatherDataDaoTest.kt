package com.chibde.weatherapp.db

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.runner.AndroidJUnit4
import com.chibde.weatherapp.util.getValueSync
import com.chibde.weatherapp.utils.Locations
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class WeatherDataDaoTest : DatabaseTest() {
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Test
    fun testWeatherDataForLocation() {
        db.weatherAppDao().insert(weatherData)
        val weatherData = db.weatherAppDao().getWeatherDataForLocation(Locations.England.id)
        assertThat(weatherData, notNullValue())
        assertThat(weatherData.size, `is`(12))
    }

    @Test
    fun testWeatherDataForYearAndLocation() {
        db.weatherAppDao().insert(weatherData)
        val weatherData = db.weatherAppDao().getWeatherData(
            year = 1910,
            location = Locations.England.id
        ).getValueSync()
        assertThat(weatherData, notNullValue())
        assertThat(weatherData.size, `is`(12))
    }

    @Test
    fun testWeatherDataForYear() {
        db.weatherAppDao().insert(weatherData)
        val weatherData = db.weatherAppDao().getWeatherData(year = 1910)
        assertThat(weatherData, notNullValue())
        assertThat(weatherData.size, `is`(12))

        val emptyWeatherData = db.weatherAppDao().getWeatherData(year = 1911)
        assertThat(emptyWeatherData.size, `is`(0))
    }
}