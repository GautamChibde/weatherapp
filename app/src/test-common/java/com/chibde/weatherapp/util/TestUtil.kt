package com.chibde.weatherapp.util

import androidx.annotation.Nullable
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.chibde.weatherapp.api.WeatherDataResponse
import com.chibde.weatherapp.db.entity.Location
import com.chibde.weatherapp.db.entity.WeatherData
import com.chibde.weatherapp.repository.RemoteWeatherDataRepository
import com.chibde.weatherapp.utils.Locations
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

object TestUtil {

    fun createDummyWeatherData(locations: Locations): List<WeatherData> {
        return (0 until 12).map {
            WeatherData(
                tmin = 1.1,
                tmax = 1.2,
                rainFall = 1.3,
                month = it,
                year = 1910,
                location = Location(
                    id = locations.id,
                    name = locations.name
                )
            )
        }.toList()
    }

    fun createDummyWeatherDataResponse(): List<WeatherDataResponse> {
        return (1 until 13).map {
            WeatherDataResponse(
                year = 1910 + it,
                month = it,
                value = 1.3
            )
        }.toList()
    }

    fun createWeatherDataFromResponse(location: Locations = Locations.UK): List<WeatherData> {
        val d1 = createDummyWeatherDataResponse()
        val d2 = createDummyWeatherDataResponse()
        val d3 = createDummyWeatherDataResponse()
        return d1.map {
            WeatherData(
                year = it.year,
                month = it.month,
                tmax = RemoteWeatherDataRepository.getValue(it.year, it.month, d2),
                rainFall = it.value,
                tmin = RemoteWeatherDataRepository.getValue(it.year, it.month, d3),
                location = Location(location.id, location.name)
            )
        }
    }
}

@Throws(InterruptedException::class)
fun <T> LiveData<T>.getValueSync(): T {
    val data = arrayOfNulls<Any>(1)
    val latch = CountDownLatch(1)
    val observer = object : Observer<T> {
        override fun onChanged(@Nullable o: T) {
            data[0] = o
            latch.countDown()
            removeObserver(this)
        }
    }
    this.observeForever(observer)
    latch.await(2, TimeUnit.SECONDS)

    return data[0] as T
}