package com.chibde.weatherapp.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.chibde.weatherapp.db.entity.WeatherData
import com.chibde.weatherapp.repository.LocalWeatherDataRepository
import com.chibde.weatherapp.utils.Locations
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val repository: LocalWeatherDataRepository
) : ViewModel() {

    var location: Int = Locations.England.id
    private var year: Int = 2001

    private val _requestData = MutableLiveData<RequestData>()

    val requestData: LiveData<RequestData>
        get() = _requestData

    val weatherData: LiveData<List<WeatherData>> = Transformations
        .switchMap(_requestData) { input ->
            repository.getWeatherDataFor(
                year = input.year,
                location = input.location
            )
        }

    fun setRequestData(year: Int?, location: Int?) {
        location?.let { this.location = it }
        year?.let { this.year = it }

        val update = RequestData(this.year, this.location)

        if (_requestData.value == update) {
            return
        }
        _requestData.postValue(update)
    }

    fun nextyear() {
        this.year += 1
        val update = RequestData(this.year, this.location)
        _requestData.postValue(update)
    }

    fun previousYear() {
        this.year -= 1
        val update = RequestData(this.year, this.location)
        _requestData.postValue(update)
    }
}

data class RequestData(
    val year: Int,
    val location: Int
)