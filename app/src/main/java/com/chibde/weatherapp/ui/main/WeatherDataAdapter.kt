package com.chibde.weatherapp.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.chibde.weatherapp.R
import com.chibde.weatherapp.db.entity.WeatherData
import com.chibde.weatherapp.utils.Utils
import kotlinx.android.synthetic.main.item_weather.view.*
import java.util.*

class WeatherDataAdapter(
    private val items: ArrayList<WeatherData>
) : RecyclerView.Adapter<WeatherDataAdapter.WeatherDataHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherDataHolder {
        return WeatherDataHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_weather, parent, false)
        )
    }

    override fun onBindViewHolder(holder: WeatherDataHolder, position: Int) {
        with(holder.itemView) {
            tv_month.text = items[position].month?.let {
                Utils.getMonthName(
                    it,
                    Locale.US,
                    false
                )
            }
            tv_tmax.text = items[position].tmax?.toString() ?: "N/A"

            tv_tmin.text = items[position].tmin?.toString() ?: "N/A"

            tv_rainfall.text = items[position].rainFall?.toString() ?: "N/A"
        }
    }

    override fun getItemCount() = items.size

    fun refresh(it: List<WeatherData>?) {
        it?.let {
            items.clear()
            items.addAll(it)
            notifyDataSetChanged()
        }
    }

    class WeatherDataHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}