package com.chibde.weatherapp.ui.main

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.list.listItemsSingleChoice
import com.chibde.weatherapp.R
import com.chibde.weatherapp.WeatherApp
import com.chibde.weatherapp.utils.AppViewModelFactory
import com.chibde.weatherapp.utils.Locations
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: AppViewModelFactory

    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (application as WeatherApp).getAppComponent().inject(this)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(MainViewModel::class.java)
        if (savedInstanceState == null) {
            viewModel.setRequestData(location = 0, year = 2001)
        }
        initViews()
        initObservers()
    }

    private fun initViews() {
        tv_title.text = getString(R.string.app_name)
        btn_location.setOnClickListener {
            MaterialDialog(this).show {
                listItemsSingleChoice(
                    items = enumValues<Locations>().map { location ->
                        location.name
                    }.toList(),
                    initialSelection = viewModel.location
                ) { _, index, _ ->
                    viewModel.setRequestData(location = index, year = null)
                }

                title(text = "Select Location")
            }
        }

        btn_next_year.setOnClickListener {
            viewModel.nextyear()
        }

        btn_previous_year.setOnClickListener {
            viewModel.previousYear()
        }

        rv_weather_data.layoutManager = LinearLayoutManager(this)
        rv_weather_data.adapter = WeatherDataAdapter(arrayListOf())
    }

    private fun initObservers() {
        viewModel.weatherData.observe(this, Observer {
            if (it.isEmpty()) {
                tv_empty.visibility = View.VISIBLE
                rv_weather_data.visibility = View.INVISIBLE
            } else {
                rv_weather_data.visibility = View.VISIBLE
                (rv_weather_data.adapter as WeatherDataAdapter).refresh(it)
                tv_empty.visibility = View.INVISIBLE
            }
        })

        viewModel.requestData.observe(this, Observer {
            btn_location.text = Locations.values()[it.location].name
            btn_year.text = it.year.toString()
        })
    }
}
