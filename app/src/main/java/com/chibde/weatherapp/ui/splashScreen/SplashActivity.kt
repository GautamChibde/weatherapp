package com.chibde.weatherapp.ui.splashScreen

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.afollestad.materialdialogs.MaterialDialog
import com.chibde.weatherapp.R
import com.chibde.weatherapp.WeatherApp
import com.chibde.weatherapp.ui.main.MainActivity
import com.chibde.weatherapp.utils.AppViewModelFactory
import javax.inject.Inject

class SplashActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: AppViewModelFactory

    private lateinit var viewModel: SplashViewModel

    private lateinit var dialog: MaterialDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (application as WeatherApp).getAppComponent().inject(this)
        setContentView(R.layout.activity_splash)

        dialog = MaterialDialog(this)
            .title(R.string.error_dialog_title)
            .cancelOnTouchOutside(false)
            .message(R.string.error_dialog_message)
            .negativeButton(R.string.cancel) {
                finish()
                dialog.dismiss()
            }
            .positiveButton(R.string.retry) {
                viewModel.getData()
                dialog.dismiss()
            }

        viewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(SplashViewModel::class.java)
        viewModel.getData()

        viewModel.dataResults.observe(this, Observer {
            if (it.success) {
                val intent = Intent(this, MainActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            } else {
                dialog.show()
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.dispose()
    }

}
