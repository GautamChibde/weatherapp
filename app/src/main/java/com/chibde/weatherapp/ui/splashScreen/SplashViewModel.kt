package com.chibde.weatherapp.ui.splashScreen

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.chibde.weatherapp.repository.RemoteWeatherDataRepository
import com.chibde.weatherapp.repository.WeatherDataResults
import javax.inject.Inject

class SplashViewModel @Inject constructor(
    private val repository: RemoteWeatherDataRepository
) : ViewModel() {

    private val _dataResults = MutableLiveData<WeatherDataResults>()

    val dataResults: LiveData<WeatherDataResults>
        get() = _dataResults

    init {
        repository.weatherDataResults.observeForever {
            if (it.success) {
                _dataResults.postValue(it)
            } else {
                _dataResults.postValue(it)
            }
        }
    }

    fun getData() {
        repository.getWeatherData()
    }

    fun dispose() {
        repository.dispose()
    }
}