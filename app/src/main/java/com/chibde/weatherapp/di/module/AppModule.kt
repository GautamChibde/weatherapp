package com.chibde.weatherapp.di.module

import android.app.Application
import androidx.room.Room
import com.chibde.weatherapp.api.ConnectivityInterceptor
import com.chibde.weatherapp.api.WeatherApiService
import com.chibde.weatherapp.db.WeatherAppDb
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class AppModule {

    @Singleton
    @Provides
    fun getRetrofitService(app: Application): WeatherApiService {
        return WeatherApiService.invoke(connectivityInterceptor = ConnectivityInterceptor(app.applicationContext))
    }

    @Singleton
    @Provides
    fun provideDb(app: Application): WeatherAppDb {
        return Room
            .databaseBuilder(app, WeatherAppDb::class.java, "weatherapp.db")
            .fallbackToDestructiveMigration()
            .build()
    }
}