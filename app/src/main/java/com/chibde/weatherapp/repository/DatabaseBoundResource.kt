package com.chibde.weatherapp.repository

import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.chibde.weatherapp.db.entity.WeatherData

abstract class DatabaseBoundResource
@MainThread constructor() {

    private val result = MediatorLiveData<List<WeatherData>>()

    init {
        @Suppress("LeakingThis")
        val dbSource = loadFromDb()
        result.addSource(dbSource) { data ->
            setValue(data.sortedBy {
                it.month
            })
        }
    }


    @MainThread
    private fun setValue(newValue: List<WeatherData>) {
        if (result.value != newValue) {
            result.value = newValue
        }
    }

    fun asLiveData() = result as LiveData<List<WeatherData>>


    @MainThread
    protected abstract fun loadFromDb(): LiveData<List<WeatherData>>

}