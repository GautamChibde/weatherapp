package com.chibde.weatherapp.repository

import androidx.lifecycle.LiveData
import com.chibde.weatherapp.db.WeatherAppDb
import com.chibde.weatherapp.db.entity.WeatherData
import com.chibde.weatherapp.testing.OpenForTesting
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@OpenForTesting
class LocalWeatherDataRepository @Inject constructor(
        private val database: WeatherAppDb
) {
    fun getWeatherDataFor(year: Int, location: Int): LiveData<List<WeatherData>> {
        return object : DatabaseBoundResource() {
            override fun loadFromDb(): LiveData<List<WeatherData>> {
                return database.weatherAppDao().getWeatherData(
                        year = year,
                        location = location
                )
            }

        }.asLiveData()
    }
}