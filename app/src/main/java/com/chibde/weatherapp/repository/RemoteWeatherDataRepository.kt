package com.chibde.weatherapp.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.chibde.weatherapp.api.WeatherApiService
import com.chibde.weatherapp.api.WeatherDataResponse
import com.chibde.weatherapp.api.exceptions.BadRequestException
import com.chibde.weatherapp.api.exceptions.InternalServerException
import com.chibde.weatherapp.api.exceptions.NoConnectivityException
import com.chibde.weatherapp.db.WeatherAppDb
import com.chibde.weatherapp.db.entity.Location
import com.chibde.weatherapp.db.entity.WeatherData
import com.chibde.weatherapp.testing.OpenForTesting
import com.chibde.weatherapp.utils.Locations
import com.chibde.weatherapp.utils.Metrics
import kotlinx.coroutines.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@OpenForTesting
class RemoteWeatherDataRepository @Inject constructor(
        private val service: WeatherApiService,
        private val database: WeatherAppDb
) {

    val _weatherDataResults = MutableLiveData<WeatherDataResults>()
    val weatherDataResults: LiveData<WeatherDataResults>
        get() = _weatherDataResults

    private var job: Job? = null

    init {
        GlobalScope.launch {
            createLocations()
        }
    }

    fun getWeatherData() {
        job = GlobalScope.launch(Dispatchers.IO) {
            createLocations()
            val deferred1 = async { getWeatherData(Locations.England) }
            val deferred2 = async { getWeatherData(Locations.Wales) }
            val deferred3 = async { getWeatherData(Locations.Scotland) }
            val deferred4 = async { getWeatherData(Locations.UK) }
            val delay = async {
                delay(1000)
                true
            }

            if (deferred1.await()
                            .and(deferred2.await())
                            .and(deferred3.await())
                            .and(deferred4.await())
                            .and(delay.await())
            ) {
                _weatherDataResults.postValue(WeatherDataResults(success = true))
            } else {
                _weatherDataResults.postValue(WeatherDataResults(success = false))
            }
        }
    }

    private fun createLocations() {
        enumValues<Locations>().forEach { location ->
            database.locationDao().insert(
                    Location(
                            id = location.id,
                            name = location.name
                    )
            )
        }
    }

    suspend fun getWeatherData(location: Locations): Boolean {
        try {
            if (database.weatherAppDao().getWeatherDataForLocation(location.id).isEmpty()) {
                val rainfallData = service.getWeatherData(
                        location = location.name,
                        metric = Metrics.RAINFALL.metric
                ).await()

                val tMaxData = service.getWeatherData(
                        location = location.name,
                        metric = Metrics.TMAX.metric
                ).await()

                val tMinData = service.getWeatherData(
                        location = location.name,
                        metric = Metrics.TMIN.metric
                ).await()
                val weatherData = rainfallData.map {
                    WeatherData(
                            year = it.year,
                            month = it.month,
                            tmax = getValue(it.year, it.month, tMaxData),
                            rainFall = it.value,
                            tmin = getValue(it.year, it.month, tMinData),
                            location = Location(location.id, location.name)
                    )
                }

                database.weatherAppDao().insert(weatherData)
            }
            return true
        } catch (e: BadRequestException) {
            Log.e("RemoteWeatherDataRepository", "error BadRequestException $e")
            return false
        } catch (e: NoConnectivityException) {
            Log.e("RemoteWeatherDataRepository", "error BadRequestException $e")
            return false
        } catch (e: InternalServerException) {
            Log.e("RemoteWeatherDataRepository", "error BadRequestException $e")
            return false
        }
    }

    fun dispose() {
        job?.let {
            if (it.isActive) {
                it.cancel()
            }
        }
    }

    companion object {
        fun getValue(year: Int?, month: Int?, weatherData: List<WeatherDataResponse>): Double? {
            return weatherData.firstOrNull {
                it.year == year && it.month == month
            }?.value
        }

    }
}

data class WeatherDataResults(
        val success: Boolean,
        val message: String = ""
)