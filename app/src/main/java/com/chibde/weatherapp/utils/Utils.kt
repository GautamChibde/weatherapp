package com.chibde.weatherapp.utils

import java.util.*

object Utils {
    fun getMonthName(index: Int, locale: Locale, shortName: Boolean): String {
        var format = "%tB"

        if (shortName)
            format = "%tb"

        val calendar = Calendar.getInstance(locale)
        calendar.set(Calendar.MONTH, index - 1)
        calendar.set(Calendar.DAY_OF_MONTH, 1)

        return String.format(locale, format, calendar)
    }
}