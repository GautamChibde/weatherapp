package com.chibde.weatherapp.utils

object Constants {
    const val BASE_URL = "https://s3.eu-west-2.amazonaws.com/"
}

enum class Locations(val id: Int) {
    UK(0),
    England(1),
    Scotland(2),
    Wales(3)
}

enum class Metrics(val metric: String) {
    TMAX("Tmax"),
    TMIN("Tmin"),
    RAINFALL("Rainfall")
}