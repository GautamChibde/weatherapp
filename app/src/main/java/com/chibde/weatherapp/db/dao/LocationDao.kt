package com.chibde.weatherapp.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.chibde.weatherapp.db.entity.Location

@Dao
abstract class LocationDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun insert(vararg location: Location)

    @Query("select * from location")
    abstract fun getAll(): List<Location>

    @Query("select * from location where id = :id")
    abstract fun getForId(id: Int): Location
}