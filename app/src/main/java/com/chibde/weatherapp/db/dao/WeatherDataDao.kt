package com.chibde.weatherapp.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.chibde.weatherapp.db.entity.WeatherData

@Dao
abstract class WeatherDataDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(vararg weatherData: WeatherData)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(weatherData: List<WeatherData>)

    @Query("select * from weather_data where year = :year and location_id = :location")
    abstract fun getWeatherData(year: Int, location: Int): LiveData<List<WeatherData>>

    @Query("select * from weather_data where year = :year")
    abstract fun getWeatherData(year: Int): List<WeatherData>

    @Query("select * from weather_data where location_id = :location")
    abstract fun getWeatherDataForLocation(location: Int): List<WeatherData>
}