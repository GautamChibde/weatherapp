package com.chibde.weatherapp.db.entity

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "weather_data")
data class WeatherData(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val year: Int?,
    val month: Int?,
    val tmax: Double?,
    val tmin: Double?,
    val rainFall: Double?,
    @Embedded(prefix = "location_") val location: Location
)