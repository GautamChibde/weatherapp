package com.chibde.weatherapp.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.chibde.weatherapp.db.dao.LocationDao
import com.chibde.weatherapp.db.dao.WeatherDataDao
import com.chibde.weatherapp.db.entity.Location
import com.chibde.weatherapp.db.entity.WeatherData

@Database(
    entities = [
        WeatherData::class,
        Location::class
    ],
    version = 1,
    exportSchema = false
)
abstract class WeatherAppDb : RoomDatabase() {
    abstract fun weatherAppDao(): WeatherDataDao

    abstract fun locationDao(): LocationDao
}