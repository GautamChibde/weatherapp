package com.chibde.weatherapp.api

data class WeatherDataResponse(
    val value: Double?,
    val year: Int?,
    val month: Int?
)